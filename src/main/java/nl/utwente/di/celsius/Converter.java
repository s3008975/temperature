package nl.utwente.di.celsius;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

public class Converter extends HttpServlet {
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
        throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        int celsius = Integer.parseInt(request.getParameter("celsius"));
        int convertedDegrees = ((celsius * 9) / 5) + 32;

        // Done with string concatenation only for the demo
        // Not expected to be done like this in the project
        out.println("<!DOCTYPE HTML>\n" +
            "<HTML>\n" +
            "<HEAD><TITLE>Celsius to Fahrenheit converter</TITLE>" +
            "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
            "</HEAD>\n" +
            "<BODY BGCOLOR=\"#FDF5E6\">\n" +
            "<h1>Fahrenheit: " + convertedDegrees +
            "</BODY></HTML>");
    }

}
